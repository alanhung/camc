# CPU Affnity Mask Calculator

A simple CPU affinity mask calculator in the terminal.

The below shell command has the same effect. Replace '0 1' with the cpu you want to set.
```bash
echo 0 1 | awk '{ for (i = 1; i <= NF; i++) res += 2^$i } END {printf("0x%X\n", res)}'
```
