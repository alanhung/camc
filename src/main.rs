use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Space sperated string of cpu numbers from 0 to 63 inclusive
    #[clap()]
    cpus: String,
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    const MAX: usize = 64;

    let split: anyhow::Result<Vec<usize>> = args
        .cpus
        .split_whitespace()
        .into_iter()
        .map(|str| match str.parse::<usize>().map_err(Into::into) {
            Ok(num) => {
                if num < MAX {
                    Ok(num)
                } else {
                    Err(anyhow::anyhow!("cpu number has to be less than 64"))
                }
            }
            x => x,
        })
        .collect();

    match split {
        Ok(split) => {
            let mut seen = [false; MAX];
            let mut res = 0;
            for num in split {
                if !seen[num] {
                    res += u64::pow(2, num as u32);
                    seen[num] = true;
                }
            }
            println!("{:#X}", res);
            Ok(())
        }
        Err(e) => Err(e),
    }
}
